var express = require('express')
var logger = require('morgan'); // Primeiro chamamos o pacote
var app = express()

app.use(logger('dev')) // Depois definimos como middleware
  
module.exports = {app}