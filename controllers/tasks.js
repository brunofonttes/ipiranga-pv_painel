require('isomorphic-fetch')

const url_Base = "http://couchdb-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br/dbhistorico/_design/clebio/_view"

module.exports = function (app) {
    return {
        api: function (request, response) {
            var mapPVs = require('../arquivos/apiMap.json');
            response.render('painel', { MapPVs: mapPVs })
        },

        job: function (request, response, io) {
        },

        Jdiario: function (request, response) {
            var attLPVs = require('../services/src/rotinas/diario/attListaPVs')
            attLPVs.run(url_Base)
            response.redirect("../");
        },

        pouch: function (request, response) {
        }
    }
}

















