const url_base = "http://couchdb-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br/dbhistorico/_design/clebio/_view"

function createRequestURLPainel(pv, hrInicio, hrFim) {
    url = url_base+"/painelalerta?startkey=["+pv+",\""+hrInicio+"\"]&endkey=["+pv+",\""+hrFim+"\"]"
    return url
}

function createRequestURL(dataIn, dataFim){
    url = url_base + '/pdvkmv?startkey=['+dataIn+',0,0]&endkey=['+dataFim+',99999999,4]&reduce=false';
    return url
}

function createRequestURLFull(){
    url = url_base + '/pdvkmv?reduce=false';
    return url
}
module.exports = {createRequestURL, createRequestURLPainel, createRequestURLFull}

