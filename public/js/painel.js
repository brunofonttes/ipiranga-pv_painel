var pvsFilter = [] //com tempo sem abastecer
var pvsPF = [] // PVs com Pro-Frotas
var pvsFilter_PF = []
var pvsFilter_nPF = []

//Converte a data recebida em AMPM
function convertToAMPM(dateStr) {
    var date = new Date(dateStr)
    var hora = date.getHours()
    var min = date.getMinutes()
    var AMPM = (hora > 11) ? "PM" : "AM";
    if (hora > 12) {
        hora -= 12;
    } else if (hora == 0) {
        hora = "12";
    }
    if (min < 10) {
        min = "0" + min;
    }
    return hora + ":" + min + " " + AMPM;
}

//Define a estrutura da página
function buildScreen(data) {
    var pvs = data
    //ceil
    // console.log(pvs.length / Math.floor(Math.sqrt(pvs.length * $(window).width() / $(window).height())))
    // console.log(pvs.length /(Math.sqrt(pvs.length * $(window).width() / $(window).height())))
    var col;
    if ($(window).width() > $(window).height()) {
        col = Math.ceil(pvs.length / (Math.sqrt(pvs.length * $(window).width() / $(window).height())))
        $('ul').css('-webkit-column-count', col);
        $('ul').css('-moz-column-count', col);
        $('ul').css('column-count', col);
    }
    else {
        col = 1;
    }
    var qtdY = Math.ceil(pvs.length / col)
    var fontsize = 0.17 * $(window).height() / qtdY
    $('ul').css('font-size', fontsize + 'px')
    $('ul li').css('padding-bottom', 0.45 * ($(window).height() - (qtdY * fontsize)) / qtdY + 'px')
    // console.log(col)
    // console.log(qtdY)
    // console.log($(window).height() / qtdY)
}

//Atualiza quantidade de PVs On
function updateQtdOn(qtd) {
    $('.tot .green').empty()
    $('.tot .green').text('On: ' + qtd);
}

function isProFrotas(pv){
    let achou = false;
    for (let pvpf in pvsPF) {
        // console.log(pv.npv, pvsPF[pvpf])
        if(pv.npv == pvsPF[pvpf]){
            achou = true;
            break;
        }
    }
    return achou;
}

function isnotProFrotas(pv){
    let achou = false;
    for (let pvpf in pvsFilter_PF) {
        // console.log(pv.npv, pvsFilter_PF[pvpf].NP)
        if(pv.npv == pvsFilter_PF[pvpf].npv){
            achou = true;
            break;
        }
    }
    return !achou;
}

// Carrega PVs na tela
function loadStatus(pvs) {
    var total = pvs.length;
    pvsFilter.length = 0
    $('#messages li').remove()
    $('#messages .btn-group').remove()
    // console.log(pvs);
    pvsFilter = pvs.filter(pv => pv != null)
    //ordena por tempo sem abastecer
    pvsFilter = pvsFilter.sort(function (a, b) { return b.ultTransm - a.ultTransm; });

    /*
    pvsFilter_PF = pvsFilter.filter(isProFrotas);
    pvsFilter_nPF = pvsFilter.filter(isnotProFrotas);

    console.log(pvsFilter_PF);
    console.log(pvsFilter_nPF)
    */

    for (let redPV in pvsFilter) {
        let npv = pvsFilter[redPV].npv;
        let tpTransm = pvsFilter[redPV].ultTransm;
        let nomePV = pvsFilter[redPV].nome;
        let bold = '';
        let pcent = 100;

        if (tpTransm <= 7) {
            pcent = Math.round(tpTransm * 100 / 7);
        }
        else {
            pcent = 100;
        }

        if (tpTransm > 7) { color = "danger" }
        else if (tpTransm <= 7 && tpTransm >= 6) { color = "red" }
        else if (tpTransm <= 5 && tpTransm >= 4) { color = "orangeT" }
        else if (tpTransm <= 3 && tpTransm >= 2) { color = "orange" }
        else { color = "yellow" }

        // tpTransm = (tpTransm == 99) ? '&infin;' : tpTransm;

        if(isProFrotas(pvsFilter[redPV]) == true){
            // console.log(pvsFilter[redPV].npv)
            // npv = '*'+npv+'*';
            bold = "bold";
        }
        

        $('.clearfix').append(
            $('<li>' +
                    '<div class="c100 p' + pcent + ' custom ' + color + '">' +
                        '<span >' + tpTransm + '</span>' +
                        '<div class="slice">' +
                            '<div class="bar"></div>' +
                            '<div class="fill"></div>' +
                        '</div>' +
                    '</div>' +
                    '<div class = "dados">'+
                        '<span class="pv '+bold+' ">' + nomePV + '</span>' +
                        '<div class="nome">'+  npv +'</div>'+
                    '</div>'+
                '</li>'
            )
        )
        if(nomePV.length>15){
            $('.pv:last').css('font-size',15/nomePV.length+'em')
            // $('.dados:last').css('padding-bottom',(14/nomePV.length)+'em')
            // console.log(3/(13/nomePV.length));
        }
        

    }

    buildScreen(pvsFilter);

    var statusGreen = total - pvsFilter.length
    updateQtdOn(statusGreen)
    $('.tot .red').empty()
    $('.tot .red').text('Off: ' + pvsFilter.length)
}

$(window).on('resize', function () {
    if (pvsFilter.length > 0)
        buildScreen(pvsFilter);
}).trigger('resize');

$(document).ready(function () {
    // var mock = [{"npv":5697,"nome":"D  Pedro Paragominas","ultTransm":6},{"npv":9368,"nome":"Da Gruta","ultTransm":12},{"npv":10961,"nome":"-","ultTransm":17},{"npv":11028,"nome":"-","ultTransm":1},{"npv":11182,"nome":"-","ultTransm":33},{"npv":11482,"nome":"-","ultTransm":12},{"npv":18451,"nome":"Tropical","ultTransm":3},{"npv":92946,"nome":"Mill Matriz","ultTransm":3},{"npv":134728,"nome":"P 99","ultTransm":23},{"npv":136495,"nome":"-","ultTransm":4},{"npv":1277554,"nome":"Estrela Dutra","ultTransm":7},{"npv":1300981,"nome":"-","ultTransm":3},{"npv":1316134,"nome":"Gat","ultTransm":3},{"npv":5697,"nome":"D  Pedro Betim","ultTransm":6},{"npv":9368,"nome":"Da Gruta","ultTransm":12},{"npv":10961,"nome":"-","ultTransm":17},{"npv":11028,"nome":"-","ultTransm":1},{"npv":11182,"nome":"-","ultTransm":33},{"npv":11482,"nome":"-","ultTransm":12},{"npv":18451,"nome":"Tropical","ultTransm":3},{"npv":92946,"nome":"Mill Matriz","ultTransm":3},{"npv":134728,"nome":"P 99","ultTransm":23},{"npv":136495,"nome":"-","ultTransm":4},{"npv":1277554,"nome":"Estrela Dutra","ultTransm":7}]
    // buildScreen(mock);
    // loadStatus(mock);
})

/// main ///
var socket = io();

socket.on('hrAtual', function (hora) {
    $('.navbar-text').text('Última Sincronização: ' + convertToAMPM(hora));
})

socket.on('tempoSTransmisao', function (TST) {
    console.log(TST);
    loadStatus(TST);
})

socket.on('qtdOn', function (qtd) {
    // console.log(qtd);
    updateQtdOn(qtd);
})

socket.on('pvsPF', function (lpvsPF) {
    pvsPF = lpvsPF;
    // console.log(lpvsPF);
})
///////////