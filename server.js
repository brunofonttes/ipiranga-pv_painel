﻿var consign = require('consign') // Primeiro chamamos o módulo
var express = require('express')
var app = express()
var http = require('http').Server(app);
var io = require('socket.io')(http)

const url_Base = "http://couchdb-replication-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br/dbhistorico/_design/clebio/_view"

app.set('view engine', 'ejs')    // Setamos que nossa engine será o ejs
app.use(express.static(__dirname + '/public'));
// app.use(function(){s.run(url_Base)})

// Antes de exportarmos o app, configuramos o consign.
consign()
    .include('controllers')
    .then('routes')
    .into(app);
//emite qtdOn apenas quando a página é carregada.
io.on('connection', function (socket) {
    let fs = require('fs');
    let fileTempoOff = './arquivos/tempoOff.json'
    try {
        let ultTempoOff = fs.readFileSync(fileTempoOff, "utf-8", function (err) { });
        let json = JSON.parse(ultTempoOff);
        io.emit('tempoSTransmisao',json.pvs);
        io.emit('hrAtual',json.hrAtt);
        io.emit('qtdOn',json.qtdOn);
    }
    catch(err){
        console.log('Nenhum histórico existente.');
    }
})

io.once('connection', function (socket) {
    let fs = require('fs');
    let util = require('util');
    let cron = require('node-cron');

    let abstPVs = require('./services/src/rotinas/minuto/abstPVs');
    // '*/5 * * * * *'
     var task = cron.schedule('* * * * *', function () {
        let file;
        const fileLPVs = './arquivos/lpvs.json';
        try {
            file = fs.readFileSync(fileLPVs, "utf-8", function (err) { })
        }
        catch (err) {
            file = fs.readFileSync(fileLPVs, { enconding: 'utf-8', flag: 'w+' })
        }
        try{
            let jsonLPVs = JSON.parse(file);
            let listaPVs = jsonLPVs.pvs;
            let mapPVs = abstPVs.getAbstPVs(io, listaPVs);
        }
        catch(err){
            console.log("Aguardando lista!")
        }
     }, false);
     task.start();
});

var port = process.env.PORT || 3006;

http.listen(port, function () {
    console.log('Servidor rodando em http://localhost:%s', port);
});

module.exports = app

