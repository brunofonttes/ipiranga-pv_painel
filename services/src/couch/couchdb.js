function getTempoSemTransmisao(pv) {
    var PouchDB = require('pouchdb');
    PouchDB.plugin(require('pouchdb-find'));
    PouchDB.debug.enable('pouchdb:find');

    var db = new PouchDB('http://couchdb-replication-kmv-pdv-caminhoneiro.ipp.openshift.locawebcorp.com.br/dbhistorico', {
        ajax: {
            cache: true,
            timeout: 120000,
        }
    });

    db.createIndex({
        index: { fields: ['abastecimento.horaAbastecimento', 'pv'] },
    })
    return new Promise((resolve, reject) => {
        var tempo = db.find({
            selector: {
                $and: [
                    {
                        'pv': { '$eq': pv }
                    },
                    {
                        'abastecimento.horaAbastecimento': { '$gt': '2017-12-01T00:00:00.000Z' }
                    }
                ]
            },

            fields: [
                'pv',
                'abastecimento.horaAbastecimento'
            ],

            // sort: [{ "abastecimento.horaAbastecimento": "asc" }],
            limit: 1
            
        })
        resolve(tempo)
        .catch(err => reject("TIMEOUT_COUCHDB"));
        // resolve("sucess").catch(err => console.log(err));
    })
}

function emitTempoSemTransmissao(pv) {
    var pt = require('promise-timeout');

    var promiseTempoTransmissao = getTempoSemTransmisao(pv)

    pt.timeout(promiseTempoTransmissao, 120000)
        .then(function (result) {
            console.log(JSON.stringify(result))
            //    return result;
        }).catch(function (err) {
            if (err instanceof pt.TimeoutError) {
                console.error('Timeout :-(');
            }
        });
}

function verificaAbstPV(resultPVsAbs) {
    console.log(resultPVsAbs.length)
    for (let pv = 0; pv < resultPVsAbs.length; pv++) {
        if (resultPVsAbs[pv].abstPV == 0) {
            console.log(resultPVsAbs[pv].npv)
            emitTempoSemTransmissao(resultPVsAbs[pv].npv)
        }
    }
}


module.exports = { verificaAbstPV }
