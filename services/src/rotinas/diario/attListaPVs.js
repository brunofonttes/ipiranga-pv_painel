function run(url_Base) {
    var fs = require('fs');
    var cron = require('node-cron');
    var request = require('request')
    var moment = require('moment-timezone')

    const fileLPVs = './arquivos/lpvs.json';
    var rr = require('../resume-reset')
    var dbPA = require('../../../../db/api_kmv')
    var exclude = (require('../../../../arquivos/exclude.json')).pvs


    function criaDataDeBusca(ultAtt) {
        let data = moment(ultAtt)
        data = data.tz('America/Sao_Paulo').format('YYYYMD');
        return data;
    }

    var options = {
        url: dbPA.createRequestURLFull()
    };

     /* ===========================================================================================
            Ordena e atualiza o arquivo lpvs.json
    ===========================================================================================*/
    function gravaLPVs(lpvs) {
        lpvs.sort(function (a, b) { return a - b });

        let ultAtt = moment();
        ultAtt = ultAtt.tz('America/Sao_Paulo');

        var fileToSave = {
            hrAtt: ultAtt,
            pvs: lpvs
        };

        fs.writeFile(fileLPVs, JSON.stringify(fileToSave), { enconding: 'utf-8', flag: 'w' }, function (err) {
            if (err) throw err;
            console.log('Arquivo salvo!');
        });
    }

    /* ===========================================================================================
            Verifica diferença entre a lista de PVs existente armazenada e a lista de PVs 
            obtida em "obtemLPVs" gerando uma nova lista de PVs atualizada.
    ===========================================================================================*/
    function verificaDiffLPVs(ult_Dia) {
        var file = fs.readFileSync(fileLPVs, 'utf8')
        var pvsParse = JSON.parse(file)
        var pvsFromBase = pvsParse.pvs;

        console.log(pvsFromBase)

        for (let pv in ult_Dia) {
            var achou = false
            /* Verifica para cada PV da lista de PVs do último período */
            /* já existe na lista de PVs armazenada */
            for (let b in pvsFromBase) {
                if (pvsFromBase[b] == ult_Dia[pv]) {
                    achou = true;
                    break;
                }
            }
            if (!achou) {
                pvsFromBase.push(ult_Dia[pv])
            }
        }
        gravaLPVs(pvsFromBase);
    }

    /* ===========================================================================================
                Gera uma lista de PVs com base no json recebido pelo parâmetro "info"
    ===========================================================================================*/
    function obtemLPVs(info, callback) {
        var listaPVs = [];
        for (let i in info.rows) {
            var del = 0;
            let npv = info.rows[i].value.npv;
            /* | Verifica se PV já existe na lista de exclusão | */
            for (let j in exclude) {
                if (npv == exclude[j]) {
                    del = 1;
                    break;
                }
            }
            if (del == 0) {
                var achou = 0;
                /* | Verifica se PV já existe na lista que está sendo criada | */
                for (let j in listaPVs) {
                    if (npv == listaPVs[j]) {
                        achou = 1;
                        break;
                    }
                }
                /* | Adiciona PV à lista de PVs caso seja a primeira ocorrência do mesmo | */
                if (achou == 0) {
                    listaPVs.push(npv);
                }
            }
        }
        callback(listaPVs);
    }

    function asyncParse(string) {
        return (new Response(string)).json();
    }

    /* ========================================================================================
            Callback para a execução da função request.get de maneira simplificada
    ===========================================================================================*/
    function callback(error, response, body) {
        asyncParse(body).then(function (info) {
            obtemLPVs(info, verificaDiffLPVs);
        })
    }

    // '59 23 * * *' 

    /* ========================================================================================
            Job de execução diária capaz de criar o arquivo lpvs.json caso não exista,
            realizar uma primeira execução varrendo toda a base e na sequência, já
            existindo uma lista armazenada, verificar apenas os registros contidos
            no dia anterior.
    ===========================================================================================*/
    var task = cron.schedule('*/30 * * * *', function () {
        console.log("\n****Passou*****\n");
        var lpvsReaded;
        try {
            lPVsReaded = fs.readFileSync(fileLPVs, "utf-8", function (err) { })
        }
        catch (err) {
            lPVsReaded = fs.readFileSync(fileLPVs, { enconding: 'utf-8', flag: 'w+' })
        }

        if (lPVsReaded.length > 0) {
            var lpvsParse = JSON.parse(lPVsReaded)

            var dataIn = criaDataDeBusca(lpvsParse.hrAtt)
            var dataFim = criaDataDeBusca()

            options.url = dbPA.createRequestURL(dataIn, dataFim);
            request.get(options, callback);

            console.log(options.url)
        }
        else {
            gravaLPVs([])

            options.url = dbPA.createRequestURLFull();

            request.get(options, callback);

            console.log(options.url);
        }
    }, false);
    task.start();
}

module.exports = { run }