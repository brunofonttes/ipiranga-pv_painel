var pnAlerta = require('./request-painel')
var tpvoff = require('./timerPvOff')
var moment = require('moment-timezone')

/* ===========================================================================================
      Obtém a quantidade de abastecimentos nas últimas 24 horas a partir do instante
      em que foi feita a chamada da função. 
  ===========================================================================================*/
async function getAbstPVs(io, listaPVs) {

    let hrAtual = moment();
    let hrInicio = hrAtual.clone().add(-1, 'hour');

    hrInicio = hrInicio.format();
    hrAtual = hrAtual.format();

    // hrInicio = hrInicio.tz('America/Sao_Paulo').format();
    // hrAtual = hrAtual.tz('America/Sao_Paulo').format();

    /* Aguarda a verificação de todos os PVs para dar seguimento ao fluxo da função  */
    let statusPVs = await Promise.all(listaPVs.map(async (pv) => {
        return pnAlerta.requestPV(pv, hrInicio, hrAtual);
    }));

    let json = JSON.stringify(statusPVs);
    console.log(json)
    tpvoff.percorreLPV(statusPVs, io)
    // fs_writeFile('arquivos/apiMap.json', json)
    //     .then(() => console.log('arquivo criado com sucesso!'))
    //     .catch(err => console.log(err))
}

module.exports = { getAbstPVs }