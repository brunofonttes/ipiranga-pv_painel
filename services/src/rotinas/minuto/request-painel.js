﻿var dbpa = require('../../../../db/api_kmv')
var req = require('request')
var moment = require('moment')

function asyncParse(string) {
    return (new Response(string)).json();
}
/* ===========================================================================================
         Realiza requisição para a origem verificando a partir do número do PV 
         Corporativo, a quantidade de abastecimentos em um dado período.
  ===========================================================================================*/
function requestPV(pv, hrIn, hrF) {
     console.log(pv, hrIn, hrF);
    var url = dbpa.createRequestURLPainel(pv, hrIn, hrF);

    return new Promise(resolve => {
        setTimeout(() => {
            req.get(url, (error, response, body) => {
                asyncParse(body).then(function (json) {
                    var mapPV = { npv: pv, abstPV: json.rows.length };
                    resolve(mapPV);
                }, 2000);
            });
        })
    })
}

/* ===========================================================================================
         Realiza requisições para a origem, verificando a partir do número do PV 
         Corporativo e de um horario inicial, até que a quantidade de abastecimentos 
         seja diferente de zero para que se obtenha o dia em que houve a última
         transmissão para a cloud.
  ===========================================================================================*/
function buscaUltimoAbstPV(npv, hrInicioBusca, nomePVs) {
    let hrAtual = moment();
    console.log('REQ: ' + npv, hrInicioBusca)

    let hrInicio = hrAtual.clone().add(-(hrInicioBusca + 1), 'day');
    let hrFim = hrAtual.clone().add(-hrInicioBusca, 'day');

    hrInicio = hrInicio.format();
    hrFim = hrFim.format();

    // hrInicio = hrInicio.tz('America/Sao_Paulo').format();
    // hrFim = hrFim.tz('America/Sao_Paulo').format();

    return new Promise(resolve => {
        setTimeout(() => {
            requestPV(npv, hrInicio, hrFim).then(function (result) {
                if (result.abstPV != 0) {
                    let nome = '-';
                    for (let pv in nomePVs) {
                        if (npv == nomePVs[pv].npv) {
                            nome = nomePVs[pv].nome;
                            console.log(nome);
                            break;
                        }
                    }
                    let datePV = { npv: npv, nome: nome, ultTransm: hrInicioBusca };
                    resolve(datePV);
                }
                else if (result.abstPV == 0) {
                    resolve(buscaUltimoAbstPV(npv, hrInicioBusca + 1));
                }
            }); 
        }, 2000);
    })
}

async function buscaNomePV(npv, nomePVs) {
    return new Promise(resolve => {
        setTimeout(() => {
            for (pv in namePVs) {
                if (npv == namePVs[pv].npv) {
                    resolve(namePVs.nome)
                }
            }
            resolve('-');
        });
    }, 1000);
}

module.exports = { requestPV, buscaUltimoAbstPV };