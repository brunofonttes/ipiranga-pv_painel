﻿var moment = require('moment-timezone');
var pnAlerta = require('./request-painel')
var rr = require('../resume-reset')

var fs = require('fs');

const fileLPVsPF = './arquivos/pvsPF.json';
const fileNomeLPVs = './arquivos/nomeLPVs.json';
const fileLPVs = './arquivos/tempoOff.json';


async function percorreLPV(statusPVs, io) {
    var iob = io;
    var hrAtual = moment().tz('America/Sao_Paulo').format();
    var resume = false;
    var file;
    var PVsOff;
    var nomePVs;

    /* Verifica se existe/é possível abrir o dado arquivo, e o cria, caso não exista */
    try {
        file = fs.readFileSync(fileLPVs, "utf-8", function (err) { });
    }
    catch (err) {
        file = fs.readFileSync(fileLPVs, { enconding: 'utf-8', flag: 'w+' })
    }

    /* Verifica se o arquivo de tempoOff lido é passível de se converter e realizar os determinados procedimentos,
    caso não seja, ativa log */

    try {
        var fileParse = JSON.parse(file);
        console.log(fileParse)
        //verifica se a última atualização ocorreu dentro do período de um dia
        if (rr.resume_reset(moment(fileParse.hrAtt))) {
            console.log("true")
            resume = true;
            PVsOff = fileParse.pvs;
        };
        // Recebe lista com nome dos PVs 
    }
    catch (err) {
        console.log("Primeira execução");
    }
    try {
        file = fs.readFileSync(fileNomeLPVs, { enconding: 'utf-8' });
        nomePVs = JSON.parse(file);
    }
    catch (err) {
        file = fs.readFileSync(fileNomeLPVs, '{"pvs": []}', { enconding: 'utf-8', flag: 'w+' });

        console.log("Arquivo de nomes inexistente.");
    }

    /* Recebe a lista de PVs e seus abastecimentos. Para os pvs com abastecimento
    igual a 0(ZERO), busca ha quanto tempo o pv está sem trasmissão. */
    var ultTransm = await Promise.all(statusPVs.map(async (pv) => {
        // defineBuscaAbst(pv, PVsOff, nomePVs, resume)
        if (pv.abstPV == 0) {
            if (resume == true) {
                let achou = false;
                let ultTransm;
                let infoPV;
                /*jsonPVs : json dos PVs sem transmissão obtido de tempoOff.json*/
                for (let jPV in PVsOff) {
                    if (PVsOff[jPV].npv == pv.npv) {
                        achou = true;
                        ultTransm = PVsOff[jPV].ultTransm; //dias
                        infoPV = PVsOff[jPV];
                        break;
                    }
                }
                /*Inicia busca por abastecimento a partir do último dia em que houve transmissão.*/
                if (achou) {
                    console.log('======' + pv.npv, ultTransm + '======')
                    if (infoPV.nome == "-") {
                        return pnAlerta.buscaUltimoAbstPV(pv.npv, ultTransm, nomePVs.pvs);
                    }
                    else {
                        let infoPVarr = [];
                        infoPVarr.push(infoPV);
                        console.log(infoPVarr);
                        return pnAlerta.buscaUltimoAbstPV(pv.npv, ultTransm, infoPVarr);
                    }
                }
                /*Inicia busca por abastecimento do "zero", no período de 1 dia, ha 1 dia.*/
                else {
                    return pnAlerta.buscaUltimoAbstPV(pv.npv, 0, nomePVs.pvs);
                }
            }
            /*Inicia busca por abastecimento do "zero", no período de 1 dia, ha 1 dia.*/
            else {
                return pnAlerta.buscaUltimoAbstPV(pv.npv, 0, nomePVs.pvs);
            }
        }
    }))

    console.log(ultTransm);
    // var hrAttAtual = hrAtt.tz('America/Sao_Paulo').format();

    //filtrando pvs sem abastecimento (!=null)
    var ultTransmFilter = ultTransm.filter(pv => pv != null)

    //obtendo quantida de PVs On
    var resultsOnLength = ultTransm.length - ultTransmFilter.length;

    //emitindo PVS com tempo sem transmissao e hora de Sincronização
    pvsPF = JSON.parse(fs.readFileSync(fileLPVsPF, "utf-8", function (err) { }));
    iob.emit('pvsPF', pvsPF.pvs);
    iob.emit('tempoSTransmisao', ultTransm);
    iob.emit('hrAtual', hrAtual);

    //gravando arquivo com PVs sem transmissao
    var objPVsOff = {
        'hrAtt': hrAtual,
        'qtdOn': resultsOnLength,
        'pvs': ultTransmFilter,
    }

    fs.writeFile('./arquivos/tempoOff.json', JSON.stringify(objPVsOff), function (err) {
        if (err) throw err;
        console.log('Arquivo salvo!');
    });
}

/* Recebe a lista de PVs e seus abastecimentos. Para os pvs com abastecimento
igual a 0(ZERO), busca ha quanto tempo o pv está sem trasmissão. */
// async function defineBuscaAbst(pv, PVsOff, nomePVs, resume) {
   
// }

module.exports = { percorreLPV };