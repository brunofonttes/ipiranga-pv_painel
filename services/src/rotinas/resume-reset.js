var moment = require('moment-timezone');
function resume_reset(ultAtt){
    var hrAtual = moment()
    hrAtual = hrAtual.tz('America/Sao_Paulo').format()
    hrAtual = moment(hrAtual)
    /*Recebe data limite de 24Hrs anteriores ao horário atual*/
    var limiteHr = moment(moment(hrAtual.clone().add(-24, 'hour')).tz('America/Sao_Paulo').format())

    console.log()
    console.log("UltimaAtt " + ultAtt.format())
    console.log("Limite " + limiteHr.format())

    /*Verifica se a última atualização é recente o suficiente para que se resuma 
    a partir do último registro */
    return ultAtt.isAfter(limiteHr, "hour")
}

module.exports = { resume_reset }